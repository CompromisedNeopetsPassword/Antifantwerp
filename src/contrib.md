---
layout: page.md
title: Contributing
---
# Contributing to Antifantwerp
Currently, this is just a website of information run by one person. But more information is good!

If you have any to add, please mail me at `antifantwerp@proton.me`
